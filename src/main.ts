import { defineCustomElement } from "vue";
import App from "./App.vue";
const customElement = defineCustomElement(App);
customElements.define("dashboard-widget", customElement);
