export enum Variants {
  primary = "btn-primary",
  secondary = "btn-secondary",
  accent = "btn-accent",
  ghost = "btn-ghost",
  link = "btn-link",
  info = "btn-info",
  success = "btn-success",
  warning = "btn-warning",
  error = "btn-error",
}

export enum Sizes {
  lg = "btn-lg",
  sm = "btn-sm",
  xs = "btn-xs",
}
