export type Int = number & { __int__: void };

export interface SocketIO {
  acks: Object;
  connected: boolean;
  disconnected: boolean;
  flags: Object;
  id: string;
  ids: Int;
  io: any;
  nsp: string;
  receiveBuffer: string[];
  sendBuffer: string[];
  subs: Function[];
  _callbacks: Function[];
}

export interface CallbackDefault {
  status: string;
  msg: any;
}

export interface CallbackLogin extends CallbackDefault {
  username: string;
}

export interface CallbackError extends CallbackDefault {}

export interface ResponseDefault {
  author: string;
  timestamp: number;
  msg: any;
}

export interface EventMsgBodyAuthenticate {
  auth: string;
}
