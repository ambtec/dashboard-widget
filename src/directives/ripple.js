const ripple = (() => {
  const rippleEffect = (event) => {
    const btn = event.currentTarget;

    const circle = document.createElement("span");
    const diameter = Math.max(btn.clientWidth, btn.clientHeight);
    const radius = diameter / 2;

    circle.style.width = circle.style.height = `${diameter}px`;
    circle.style.left = `${event.clientX - (btn.offsetLeft + radius)}px`;
    circle.style.top = `${event.clientY - (btn.offsetTop + radius)}px`;
    circle.classList.add("ripple", "invert");
    if (btn.rippleClasses)
      circle.classList.add(...btn.rippleClasses.split(" "));

    const ripple = btn.getElementsByClassName("ripple")[0];
    if (ripple) ripple.remove();

    btn.appendChild(circle);
  };

  return {
    mounted: (el, binding) => {
      if (binding.value) el.rippleClasses = binding.value;
      return el.addEventListener("click", rippleEffect);
    },
    unmount: (el) => el.removeEventListener("click", rippleEffect),
  };
})();

export default ripple;
