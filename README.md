<div align="center">
  <a href="https://ambtec.com" title="ambtec" target="_blank">
    <img src="./assets/ambtec-logo-gitlab.png" width="128" />
  </a>
  <p><i>create shape improve</i></p>
</div>

<hr />

# Dashboard Widget

A standalone web component based on vue.js. Require running instances of [IAM service](https://gitlab.com/ambtec/iam-service) for user login and [Orchestrator service](https://gitlab.com/ambtec/orchestrator-service) for socket support.

Dashboard Widget - Coummincation flow \
![Dashboard Widget - Coummincation flow](./assets/Dashboard-Widget.drawio.png "Dashboard Widget - Coummincation flow") \
(Made with https://app.diagrams.net/)

- 1: HTTP authentication request to IAM Service
- 2: HTTP authentication response of IAM Service
- 3: On authentication success: Bidirectional Socket Stream of/to Socket Service

Successfull Authentication is required to establish a socket stream (3). See [IAM service](https://gitlab.com/ambtec/iam-service) for more information of how to setup a user.

Provide a basic socket setup for near time communication from/to server.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### ENV Configuration

- `` redirecturi ``: Expect a IAM redirect url after login. Default: http://dashboard-widget.localhost
- `` stylesheet ``: Expect a public stylesheet. Default: https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.2.19/tailwind.min.css

## IAM Account
You'll need an IAM user account to login. Follow the describtion of the [IAM service](https://gitlab.com/ambtec/iam-service) documentation to create one.

![IAM user login](./assets/IAM-User-Login-Demo.png "IAM user login") \
(1. IAM user login)

### Test board
The test board purpose is to showcase the various socket events and feedback received by the socket server.

![Dashboard controls](./assets/Dashboard-Panel.png "Dashboard controls") \
(2. Dashboard controls)

The Dashboard command panel offers various controls and feedback mechanism:
- *Send: To all* \
A message send to all logged in users. Require IAM group `` /SocketMsg ``.
- *Send: Only to yourseld* \
A message send only to your user. Require IAM group `` /SocketMsg ``.
- *Send: To all but yourself* \
A message send to every user but not yours. Require IAM group `` /SocketMsg ``.
- *Join: Test room* / *Send: Test room* / *Leave: Test room* \
Manage room access to enter, communicate and leave a specific room. Only user inside a room can see the messages of the specific room. Require IAM group `` /SocketRoomMsg ``.
- *Join: Service event room* / *Leave: Service event room* \
Consumes messages of IAM events. Require IAM group `` /DashboardEvents ``.
- *History* \
Shows received messages from the socket stream.
- *Logout* \
Logs out of IAM and Socket Service.

## License
MIT
